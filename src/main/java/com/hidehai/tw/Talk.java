package com.hidehai.tw;

import java.util.Date;

/**
 *
 * 会话模型
 * Created by hidehai on 2015/8/17.
 *
 */
public class Talk {

    /*开始时间*/
    private Date beginDate;
    /*主题*/
    private String subject;
    /*预计时间*/
    private Integer minutes;

    private boolean lightning;

    public Talk(String subject,Integer minutes){
        this.subject = subject;
        this.minutes = minutes;
    }

    public Talk(Date beginDate,String subject,Integer minutes){
        this.beginDate = beginDate;
        this.subject = subject;
        this.minutes = minutes;
    }

    public Integer getMinutes() {
        return minutes;
    }

    public void setMinutes(int minutes) {
        this.minutes = minutes;
    }

    public String getSubject() {
        return subject;
    }

    public void setSubject(String name) {
        this.subject = name;
    }

    public Date getBeginDate() {
        return beginDate;
    }

    public void setBeginDate(Date beginDate) {
        this.beginDate = beginDate;
    }

    public boolean isLightning() {
        return lightning;
    }

    public void setLightning(boolean lightning) {
        this.lightning = lightning;
    }
}
