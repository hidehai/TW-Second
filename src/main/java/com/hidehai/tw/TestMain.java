package com.hidehai.tw;

import junit.framework.Assert;

import java.io.File;
import java.io.FileNotFoundException;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.*;

/**
 * 测试入口
 * 测试内容入口： /resource/config/test
 * Created by hidehai on 2015/8/17.
 */
public class TestMain {


    public static void main(String[] args) throws ParseException {
        String path = Thread.currentThread().getContextClassLoader().getResource("") +"config"+File.separator;
        path= path.substring(6,path.length());
        Map<String,List<Talk>> result= CtmCalc.process(readInput(path,"test"));
        printMap(result);
    }

    private static String[] readInput(String path,String name){
        try {
            List<String> list = new ArrayList<String>();
            Scanner scanner = new Scanner(new File(path+name));
            while(scanner.hasNextLine()){
                list.add(scanner.nextLine());
            }
            String[] sss =(String[]) list.toArray(new String[0]);
            return sss;
        } catch (FileNotFoundException e) {
            e.printStackTrace();
        }
        return null;
    }

    /*Output*/
    private static void print(List<Talk> talks){
        Assert.assertNotNull(talks);
        for(Talk talk : talks){
            Date beginDate = talk.getBeginDate();
            SimpleDateFormat simpleDateFormat = new SimpleDateFormat("HH:mm aaa");
            System.out.println(
                    String.format("%s %s %s",beginDate == null? "暂无安排！" : simpleDateFormat.format(beginDate),
                            talk.getSubject(),talk.getMinutes()!=null? talk.getMinutes()+"min":""));
        }
    }

    private static void printMap(Map<String, List<Talk>> listMap){
        for(Map.Entry<String,List<Talk>> entry : listMap.entrySet()){
            System.out.println(entry.getKey());
            print(entry.getValue());
        }
    }
}
