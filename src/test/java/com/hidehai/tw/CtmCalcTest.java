package com.hidehai.tw;

import junit.framework.Assert;
import org.junit.Before;
import org.junit.Test;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;

/**
 * 单元测试
 * Created by hidehai on 2015/8/17.
 */
public class CtmCalcTest {

    private static String[] proposals;
    private static String[] proposals1;

    @Before
    public void init(){
       proposals = new String[]{
                "Writing Fast Tests Against Enterprise Rails 60min",
                "Overdoing it in Python 45min",
                "Lua for the Masses 30min",
                "Ruby Errors from Mismatched Gem Versions 45min",
                "Common Ruby Errors 45min",
               "Sit Down and Write 30min",
               "Pair Programming vs Noise 45min",
               "Rails Magic 60min",
               "Ruby on Rails: Why We Should Move On 60min",
               "Clojure Ate Scala (on my project) 45min",
               "Programming in the Boondocks of Seattle 30min",
               "Ruby vs. Clojure for Back-End Development 30min",
               "Ruby on Rails Legacy App Maintenance 60min",
                "Rails for Python Developers lightning",
                "Communicating Over Distance 60min",
                "Accounting-Driven Development 45min",
                "Woah 30min",
                "A World Without HackerNews 30min",
                "User Interface CSS in Rails Apps 30min"
        };

        proposals1 = new String[]{
                "Writing Fast Tests Against Enterprise Rails 60min",
                "Overdoing it in Python 45min",
                "Ruby on Rails: Why We Should Move On 60min",
                "Clojure Ate Scala (on my project) 45min",
                "Programming in the Boondocks of Seattle 30min",
                "Ruby vs. Clojure for Back-End Development 30min",
                "Ruby on Rails Legacy App Maintenance 60min",
                "Rails for Python Developers lightning",
                "Communicating Over Distance 60min",
                "Accounting-Driven Development 45min",
                "Woah 30min",
                "Lua for the Masses 30min",
                "Ruby Errors from Mismatched Gem Versions 45min",
                "Common Ruby Errors 45min",
                "Sit Down and Write 30min",
                "Pair Programming vs Noise 45min",
                "Rails Magic 60min",
                "A World Without HackerNews 30min",
                "User Interface CSS in Rails Apps 30min"
        };
    }

    /*测试数据封装*/
    @Test
    public void testAdd1(){
        Talk talk = new Talk("Writing Fast Tests Against Enterprise Rails",60);
        print(CtmCalc.calc(talk));
    }

    /*测试数据*/
    @Test
    public void testAdd2() throws ParseException {
        print(CtmCalc.calc(proposals));
    }

    /*Test for lunch*/
    @Test
    public void testLunchTime() throws ParseException {
       String[] proposalsArgs = new String[]{
               "Lunch 60min",
               "Lunch 60min"
       };
        List<Talk> talks = CtmCalc.calc(proposalsArgs);
        SimpleDateFormat simpleDateFormat = new SimpleDateFormat("HH:mm");
        for(Talk talk : talks){
            Assert.assertEquals(talk.getBeginDate(),simpleDateFormat.parse("12:00"));
        }
    }

    /*Test for network event */
    @Test
    public void testNetworking() throws ParseException {
        String[] proposalsArgs = new String[]{
                "Networking Event"
        };
        List<Talk> talks = CtmCalc.calc(proposalsArgs);
        SimpleDateFormat simpleDateFormat = new SimpleDateFormat("HH:mm");
        Assert.assertNotNull(talks);
        for(Talk talk : talks){
            Date date = talk.getBeginDate();
            Assert.assertNotNull(date);
            Assert.assertTrue(date.after(parseString("15:59")));
            Assert.assertTrue(date.before(parseString("17:01")));
        }
    }

    @Test
    public void testGlobalRule() throws ParseException {
        List<Talk> talks = CtmCalc.calc(proposals);
        print(talks);
        for(Talk talk:talks){
            if(talk.getBeginDate().equals(parseString("12:00"))){
                Assert.assertEquals(talk.getSubject(), "Lunch");
            }else if(talk.getBeginDate().equals(parseString("17:00"))){
                Assert.assertEquals(talk.getSubject(), "Networking Event");
            }
        }
    }

    @Test
    public void testTidy() throws ParseException {
        List<Talk> talks = CtmCalc.convertTalk(proposals);
        print(talks);
    }

    @Test
    public void testTidy2() throws ParseException {
        List<Talk> talks = CtmCalc.convertTalk(proposals1);
        Date beginDate = parseString("09:00");
        Date endDate = parseString("12:00");
        List<Talk> session = CtmCalc.tidy(talks, beginDate,endDate);
        print(session);
        for(Talk talk:session){
            Assert.assertTrue(talk.getBeginDate().equals(beginDate) || talk.getBeginDate().after(beginDate));
            Assert.assertTrue(talk.getBeginDate().equals(endDate) || talk.getBeginDate().before(endDate));
        }
    }


    @Test
    public void testProcess() throws ParseException {
        Map<String,List<Talk>> result= CtmCalc.process(proposals1);
        printMap(result);
    }

    /*Output*/
    private void print(List<Talk> talks){
        Assert.assertNotNull(talks);
        for(Talk talk : talks){
            Date beginDate = talk.getBeginDate();
            SimpleDateFormat simpleDateFormat = new SimpleDateFormat("HH:mm aaa");
            System.out.println(
                    String.format("%s %s %s",beginDate == null? "暂无安排！" : simpleDateFormat.format(beginDate),
                            talk.getSubject(),talk.getMinutes()!=null? talk.getMinutes()+"min":""));
        }
    }

    private void printMap(Map<String,List<Talk>> listMap){
        for(Map.Entry<String,List<Talk>> entry : listMap.entrySet()){
            System.out.println(entry.getKey());
            print(entry.getValue());
        }
    }

    /*parse String2Date*/
    private Date parseString(String hhmm){
        SimpleDateFormat simpleDateFormat = new SimpleDateFormat("HH:mm");
        try {
            return simpleDateFormat.parse(hhmm);
        } catch (ParseException e) {
            e.printStackTrace();
        }
        return null;
    }

}
